const express = require("express");
const bodyParser = require("body-parser");
const { MongoClient } = require("mongodb");
const app = express();

app.use(bodyParser.json());

MongoClient.connect(
  "mongodb+srv://safihaider:12345@cluster0.vquxu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
)
  .then((client) => {
    const employeesCollection = client.db("vend").collection("employees");

    app.get("/get_employee/:id", async (req, res) => {
      const employee = await employeesCollection
        .find({ employeeId: parseInt(req.params.id) })
        .toArray();
      res.send(employee);
    });

    app.post("/post_employee", async (req, res) => {
      const employees = await employeesCollection.find().toArray();

      emp = { ...req.body, employeeId: employees.length + 1 };

      employeesCollection.insertOne(emp).then(() => {
        res.send(emp);
      });
    });

    app.put("/update_employee/:id", async (req, res) => {
      employeesCollection
        .findOneAndUpdate(
          { employeeId: parseInt(req.params.id) },
          {
            $set: {
              name: req.body.name,
              age: req.body.age,
            },
          },
          {
            upsert: true,
          }
        )
        .then(() => {
          res.send(req.body);
        });
    });

    app.delete("/delete_employee/:id", async (req, res) => {
      const employee = await employeesCollection.deleteOne({
        employeeId: parseInt(req.params.id),
      });
      res.send(employee);
    });

    app.listen(8082, function () {
      console.log("listening on 8082");
    });
  })
  .catch((error) => console.error(error));
